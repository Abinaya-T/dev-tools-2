/* Step 1: Adding Submission column header to the table -
 *         a. Find the table header element.
 *         b. Create the Submission header <th> element.
 *         c. Add this element to the header.
 *              - Add the element only when this column doesn't exist (Optional)
 */


function addSubmissionColumnHeader() {

    var th = document.createElement("TH");
    th.setAttribute("class","reactable-th-status reactable-header-sortable ");
    th.setAttribute("role","button");
    th.setAttribute("tabindex","0");
    var x = document.createElement("STRONG");
    x.innerText = "Submissions";
    th.appendChild(x);
    var tr = document.querySelector("#question-app > div > div:nth-child(2) > div.question-list-base > div.table-responsive.question-list-table > table > thead > tr");
    tr.appendChild(th);

}


/* Step 2: Find the API endpoint for retrieving all problems
 */
function getApiUrl() {
    let url = new URL("https://leetcode.com/api/problems/all/");
    return url.toString();
    
}

/* Step 3: Get all the problems as an Array in the following object format by using fetch -
 *          {
 *              id: "",
 *              total_submitted: "",
 *              total_acs: ""
 *          }
 */
async function getAllProblems(apiUrl) {

        let response = await fetch(apiUrl);
        let allProblems = await response.json();

        const allProb = allProblems.stat_status_pairs
                    .map(val =>({ id: val.stat.frontend_question_id,  total_acs: val.stat.total_acs, total_submitted: val.stat.total_submitted}));
        return allProb;            
    }



/* Step 4: Getting every problem's row in the form of an array
 */
function getAllProblemRowElements() {

    const allprobrow = document.querySelectorAll("#question-app > div > div:nth-child(2) > div.question-list-base > div.table-responsive.question-list-table > table > tbody.reactable-data > tr");
    const allrows = Array.from(allprobrow);
    return allrows;

}

// let allProblemRowElements = getAllProblemRowElements();
// let allProblem = await getAllProblems(apiUrl);

/* Step 5: Adding total_acs / total_submitted to each row element of the table on the page. 
Iterate through each row element and add a new <td> containing the submission data in the provided format
 */
function addSubmissionsToEachProblem(allProblemRowElements, allProblems) {

        allProblemRowElements.forEach((element,i) => {
        
        let problemRowSingle = element;
        var res = allProblems.find(obj => {
            return obj.id == i+1
          });
        let total_acs = res.total_acs;
        let total_submitted = res.total_submitted;
        let submission = total_acs.toString() + "/" + total_submitted.toString();
        var td = document.createElement("TD");
        td.innerText = submission;
        problemRowSingle.appendChild(td);
    });

}



/* Step 6: Putting it all together
 */
async function createSubmissionColumnForLeetCode() {

    let apiUrl = getApiUrl();
    let allProblems = await getAllProblems(apiUrl);
    addSubmissionColumnHeader();
    let allProblemRowElements = await getAllProblemRowElements();
    addSubmissionsToEachProblem(allProblemRowElements, allProblems);
    
}

/* Step 7: Additional code for making script tampermonkey ready. This is done so that the script is properly executed when we visit https://leetcode.com/problemset/all/
 */
// let tableCheck = setInterval(() => {
// } , 100);

let tableCheck = setInterval(myTimer, 100);
function myTimer()
{
 let t = document.querySelector("#question-app > div > div:nth-child(2) > div.question-list-base > div.table-responsive.question-list-table > table")
    if(t) {
        createSubmissionColumnForLeetCode();
    }
}

function myStopFunction() {
    clearInterval(tableCheck);
  }
module.exports = {getApiUrl, getAllProblems, addSubmissionColumnHeader, getAllProblemRowElements, addSubmissionsToEachProblem, createSubmissionColumnForLeetCode};
